from django.db import models
from django_extensions.db.models import TimeStampedModel


class ParkingLot(models.Model):
    name = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    postcode = models.CharField(max_length=8)

    @property
    def capacity(self):
        return sum(self.segments.values_list("capacity", flat=True))

    @property
    def free_spaces(self):
        no_of_occupied = sum([e.occupied for e in self.segments.all()])
        return self.capacity - no_of_occupied

    @property
    def updated(self):
        return min([e.last_update for e in self.segments.all()])

    def __str__(self):
        return self.name


class Segment(models.Model):
    parking_lot = models.ForeignKey(ParkingLot, related_name="segments", on_delete=models.CASCADE)
    capacity = models.IntegerField()

    @property
    def occupied(self):
        return self.count_history.latest('created').count

    @property
    def last_update(self):
        return self.count_history.latest("created").created

    @property
    def free_spaces(self):
        return self.capacity - self.occupied

    def __str__(self):
        index = list(self.parking_lot.segments.all()).index(self) + 1
        return f"Segment {index} - ({self.parking_lot.postcode})"


class CountRecord(TimeStampedModel):
    segment = models.ForeignKey(Segment, related_name="count_history", on_delete=models.CASCADE)
    count = models.IntegerField()

    def __str__(self):
        return f"Count Record ({self.id})"
