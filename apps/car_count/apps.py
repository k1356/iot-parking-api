from django.apps import AppConfig


class CarCountConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.car_count'
