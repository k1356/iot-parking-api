import io
from types import SimpleNamespace

import pytest
from model_bakery import baker
from django.urls import reverse

from apps.car_count.models import CountRecord


@pytest.mark.django_db
def test_count_post_create_signal(client, mocker):
    parking_lot = baker.make("car_count.ParkingLot")
    baker.make("car_count.Segment", parking_lot=parking_lot, capacity=4)
    with open(" apps/car_count/tests/parked_cars.jpg", "rb") as f:
        img = f.read()
    mock_request = SimpleNamespace(content={img})
    mocker.patch("apps.car_count.views.requests.get", return_value=mock_request)

    data = dict(
        file=(io.BytesIO(b'my file contents'), "work_order.123"),
    )
    client.post(
        reverse("segments-count-cars", kwargs={"parking_lots_pk": 1, "pk": 1}),
        file=(io.BytesIO(b'my file contents'), "work_order.123"),
        content_type='multipart/form-data',
    )
    parking_lot = baker.make("car_count.ParkingLot")
    segment = baker.make("car_count.Segment", parking_lot=parking_lot, capacity=10)
    CountRecord.objects.create(segment=segment, count=8)
    assert segment.count == 3
    assert parking_lot.free_spaces == 1
