from django.urls import path, include
from rest_framework_nested import routers

from apps.car_count.views import ParkingLotViewSet, SegmentViewSet

router = routers.SimpleRouter()
router.register(r'parking_lots', ParkingLotViewSet)

segment_router = routers.NestedSimpleRouter(router, r'parking_lots', lookup='parking_lots')
segment_router.register(r'segments', SegmentViewSet, basename='segments')


urlpatterns = [
    path("", include(router.urls), name="parking_lots"),
    path("", include(segment_router.urls), name="segments"),
]
