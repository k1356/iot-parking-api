from django.contrib import admin

from apps.car_count.models import ParkingLot, Segment, CountRecord


@admin.register(ParkingLot)
class ParkingLotAdmin(admin.ModelAdmin):
    list_display = ["__str__", "city", "postcode", "capacity", "free_spaces", "updated"]
    search_fields = ["name", "city", "postcode"]
    list_filter = ["city"]

    def capacity(self, obj):
        return obj.capacity

    def free_spaces(self, obj):
        return obj.free_spaces

    def updated(self, obj):
        return obj.updated


@admin.register(Segment)
class SegmentAdmin(admin.ModelAdmin):
    list_display = ["__str__", "parking_lot", "capacity", "occupied"]
    search_fields = ["parking_lot__name", "parking_lot__city", "parking_lot__postcode"]
    list_filter = ["parking_lot__name"]

    def occupied(self, obj):
        return obj.occupied

    def has_change_permission(self, request, obj=None):
        return False


@admin.register(CountRecord)
class CountRecordAdmin(admin.ModelAdmin):
    list_display = ["__str__", "count", "segment"]
    list_filter = ["segment__parking_lot__name"]

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
